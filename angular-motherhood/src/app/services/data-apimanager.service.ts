import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { retry, catchError, tap, publishReplay, refCount, take } from 'rxjs/operators';
import { AlertService } from '../shared/components/alert-notifications';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataAPIManagerService {

  private REST_API_SERVER = "http://localhost:8000";  // Re-factor to config file?
  private max_retry_attempts_if_error = 3;

  constructor(
    private client: HttpClient,
    protected alertService: AlertService,
  ) {}

  // Consider re-factor with HttpClient interceptors.
  handleError(error: HttpErrorResponse) {
    let errorMessage = `Unknown error.`; 
    if (error.error instanceof ErrorEvent) {
      // Client-side
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side
      errorMessage = `Error Code: ${error.status}\nError Message: ${error.message}`;
    }
    this.alertService.error(errorMessage);

    return throwError(errorMessage);
  
  }

  // Generic CRUD support.
  public getDataFromAPIServer(url: string, params?: HttpParams) {
    const full_url = this.REST_API_SERVER + url;
    return this.client.get<any[]>(full_url, {params: params}).pipe(
                                                                  retry(this.max_retry_attempts_if_error), 
                                                                  publishReplay(1),  // https://stackoverflow.com/questions/49797910/angular-5-caching-http-service-api-calls
                                                                  refCount(),
                                                                  take(1),
                                                                  catchError(this.handleError)
                                                                  );
  }

  public postDataToAPIServer(url: string, obj_to_add: any): Observable<any> {
    const full_url = this.REST_API_SERVER + url;
    return this.client.post<any>(
      full_url,
      obj_to_add,
      httpOptions,
    ).pipe(retry(this.max_retry_attempts_if_error), 
           catchError(this.handleError),
           tap(
             success => this.alertService.info("Record successfully added.", { 'autoClose': true }),
           )
      );
  }

  public putDataToAPIServer(url: string, obj_to_update: any): Observable<any> {
    const full_url = this.REST_API_SERVER + url;
    return this.client.put<any>(
      full_url,
      obj_to_update,
      httpOptions,
    ).pipe(retry(this.max_retry_attempts_if_error), 
           catchError(this.handleError),
           tap(
             success => this.alertService.info("Record successfully updated.", { 'autoClose': true }),
           )
      );
  }

  public deleteDataFromAPIServer(url: string, content?: any) {
    const full_url = this.REST_API_SERVER + url;
    const deleteHttpOptions = {
                                headers: new HttpHeaders({
                                  'Content-Type': 'application/json',
                                }), 
                                body: content
                              };
    return this.client.delete<any>(full_url, deleteHttpOptions).pipe(retry(this.max_retry_attempts_if_error), 
                                                                catchError(this.handleError),
                                                                tap(
                                                                  success => this.alertService.info("Record successfully deleted.", { 'autoClose': true }),
                                                                ));
  }
}
